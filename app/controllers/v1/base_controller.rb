module V1
  class BaseController < ApplicationController
    respond_to :json

    before_action :authenticate_account_from_token!
    before_action :authenticate_account!

    private

    def authenticate_account_from_token!
      authenticate_with_http_token do |token, options|
        account_email = options[:email].presence
        account = account_email && Account.find_by_email(account_email)

        if account && Devise.secure_compare(account.authentication_token, token)
          sign_in account, store: false
        end
      end
    end
  end
end
