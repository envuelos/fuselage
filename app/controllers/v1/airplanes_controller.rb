module V1
  class AirplanesController < BaseController
    def index
      render json: Airplane.all
    end

    def show
      airplane = Airplane.find(params[:id])
      render json: airplane
    end

    def create
      airplane = Airplane.new(airplane_params)
      if airplane.save
        render json: airplane, status: :created
      else
        render json: { errors: airplane.errors }, status: :unprocessable_entity
      end
    end

    def update
      airplane = Airplane.find(params[:id])
      if airplane.update_attributes(airplane_params)
        render json: airplane, status: :ok
      else
        render json: { errors: airplane.errors }, status: :unprocessable_entity
      end
    end

    private

    def airplane_params
      params.require(:airplane).permit(:brand, :model, :registration)
    end
  end
end
