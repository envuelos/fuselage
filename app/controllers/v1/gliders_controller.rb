module V1
  class GlidersController < BaseController
    def index
      render json: Glider.all
    end

    def show
      glider = Glider.find(params[:id])
      render json: glider
    end

    def create
      glider = Glider.new(glider_params)
      if glider.save
        render json: glider, status: :created
      else
        render json: { errors: glider.errors }, status: :unprocessable_entity
      end
    end

    def update
      glider = Glider.find(params[:id])
      if glider.update_attributes(glider_params)
        render json: glider, status: :ok
      else
        render json: { errors: glider.errors }, status: :unprocessable_entity
      end
    end

    private

    def glider_params
      params.require(:glider).permit(:brand, :model, :registration)
    end
  end
end
