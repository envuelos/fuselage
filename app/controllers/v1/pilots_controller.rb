module V1
  class PilotsController < BaseController
    def index
      render json: Pilot.all
    end

    def show
      pilot = Pilot.find(params[:id])
      render json: pilot
    end

    def create
      pilot = Pilot.new(pilot_params)
      if pilot.save
        render json: pilot, status: :created
      else
        render json: { errors: pilot.errors }, status: :unprocessable_entity
      end
    end

    def update
      pilot = Pilot.find(params[:id])
      if pilot.update_attributes(pilot_params)
        render json: pilot, status: :ok
      else
        render json: { errors: pilot.errors }, status: :unprocessable_entity
      end
    end

    private

    def pilot_params
      params.require(:pilot).permit(:first_name, :last_name, :email)
    end
  end
end
