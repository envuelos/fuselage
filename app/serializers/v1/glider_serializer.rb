module V1
  class GliderSerializer < ActiveModel::Serializer
    attributes :id, :brand, :model, :registration
  end
end
