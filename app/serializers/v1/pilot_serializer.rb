module V1
  class PilotSerializer < ActiveModel::Serializer
    attributes :id, :first_name, :last_name
  end
end
