module V1
  class AirplaneSerializer < ActiveModel::Serializer
    attributes :id, :brand, :model, :registration
  end
end
