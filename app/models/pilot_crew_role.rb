class PilotCrewRole < ActiveRecord::Base
  belongs_to :pilot
  belongs_to :crew_role
end
