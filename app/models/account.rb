class Account < ActiveRecord::Base
  has_one :pilot

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  before_create :ensure_authentication_token

  private

  def ensure_authentication_token
    self.authentication_token = generate_authentication_token
  end

  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless self.class.find_by(authentication_token: token).present?
    end
  end
end
