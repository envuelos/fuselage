class FlightTypeCrewRole < ActiveRecord::Base
  belongs_to :flight_type
  belongs_to :crew_role
end
