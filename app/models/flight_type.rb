class FlightType < ActiveRecord::Base
  validates :identifier, uniqueness: true, presence: true
  has_many :flight_type_crew_roles
  has_many :crew_roles, through: :flight_type_crew_roles
end
