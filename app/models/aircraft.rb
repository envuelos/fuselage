class Aircraft < ActiveRecord::Base
  validates :brand, :model, :registration, presence: true
  GLIDER_TYPE_NAME = 'glider'
  AIRPLANE_TYPE_NAME = 'airplane'
end
