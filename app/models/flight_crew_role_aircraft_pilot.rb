class FlightCrewRoleAircraftPilot < ActiveRecord::Base
  belongs_to :flight
  belongs_to :crew_role
  belongs_to :aircraft
  belongs_to :pilot
end
