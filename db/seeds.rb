# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

if Rails.env.development?
  Pilot.find_or_create_by!(
    first_name: 'Klaus', last_name: 'Ohlmann'
  )
end

%w(glider_pilot glider_instructor airplane_pilot towplane_instructor).each do |identifier|
  CrewRole.find_or_create_by!(identifier: identifier)
end

FlightType.create!(
  identifier: 'glider_license_test',
  aircraft_types: [Aircraft::GLIDER_TYPE_NAME, Aircraft::AIRPLANE_TYPE_NAME],
  crew_roles: CrewRole.where(identifier: %w(glider_pilot airplane_pilot))
)

FlightType.create!(
  identifier: 'solo',
  aircraft_types: [Aircraft::GLIDER_TYPE_NAME, Aircraft::AIRPLANE_TYPE_NAME],
  crew_roles: CrewRole.where(identifier: %w(glider_pilot glider_instructor airplane_pilot))
)

FlightType.create!(
  identifier: 'cross_country',
  aircraft_types: [Aircraft::GLIDER_TYPE_NAME, Aircraft::AIRPLANE_TYPE_NAME],
  crew_roles: CrewRole.where(identifier: %w(glider_pilot airplane_pilot))
)

FlightType.create!(
  identifier: 'passenger_flight',
  aircraft_types: [Aircraft::GLIDER_TYPE_NAME, Aircraft::AIRPLANE_TYPE_NAME],
  crew_roles: CrewRole.where(identifier: %w(glider_pilot airplane_pilot))
)

FlightType.create!(
  identifier: 'training',
  aircraft_types: [Aircraft::GLIDER_TYPE_NAME, Aircraft::AIRPLANE_TYPE_NAME],
  crew_roles: CrewRole.where(identifier: %w(glider_pilot airplane_pilot))
)

FlightType.create!(
  identifier: 'airplane_navigation',
  aircraft_types: [Aircraft::AIRPLANE_TYPE_NAME],
  crew_roles: CrewRole.where(identifier: %w(airplane_pilot))
)

FlightType.create!(
  identifier: 'soaring',
  aircraft_types: [Aircraft::GLIDER_TYPE_NAME, Aircraft::AIRPLANE_TYPE_NAME],
  crew_roles: CrewRole.where(identifier: %w(glider_pilot airplane_pilot))
)

FlightType.create!(
  identifier: 'glider_instruction',
  aircraft_types: [Aircraft::GLIDER_TYPE_NAME, Aircraft::AIRPLANE_TYPE_NAME],
  crew_roles: CrewRole.where(identifier: %w(glider_pilot glider_instructor airplane_pilot))
)

FlightType.create!(
  identifier: 'glider_adaptation',
  aircraft_types: [Aircraft::GLIDER_TYPE_NAME, Aircraft::AIRPLANE_TYPE_NAME],
  crew_roles: CrewRole.where(identifier: %w(glider_pilot glider_instructor airplane_pilot))
)

FlightType.create!(
  identifier: 'airplane_instruction',
  aircraft_types: Aircraft::AIRPLANE_TYPE_NAME,
  crew_roles: CrewRole.where(identifier: %w(airplane_pilot airplane_instructor))
)
