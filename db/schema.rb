# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160118203324) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "authentication_token",                null: false
  end

  add_index "accounts", ["authentication_token"], name: "index_accounts_on_authentication_token", unique: true, using: :btree
  add_index "accounts", ["email"], name: "index_accounts_on_email", unique: true, using: :btree
  add_index "accounts", ["reset_password_token"], name: "index_accounts_on_reset_password_token", unique: true, using: :btree

  create_table "aircrafts", force: :cascade do |t|
    t.string   "brand"
    t.string   "model"
    t.string   "registration"
    t.string   "year"
    t.string   "type"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "crew_roles", force: :cascade do |t|
    t.string   "identifier", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "crew_roles", ["identifier"], name: "index_crew_roles_on_identifier", unique: true, using: :btree

  create_table "flight_crew_role_aircraft_pilots", force: :cascade do |t|
    t.integer  "flight_id"
    t.integer  "crew_role_id"
    t.integer  "aircraft_id"
    t.integer  "pilot_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "flight_crew_role_aircraft_pilots", ["aircraft_id"], name: "index_flight_crew_role_aircraft_pilots_on_aircraft_id", using: :btree
  add_index "flight_crew_role_aircraft_pilots", ["crew_role_id"], name: "index_flight_crew_role_aircraft_pilots_on_crew_role_id", using: :btree
  add_index "flight_crew_role_aircraft_pilots", ["flight_id"], name: "index_flight_crew_role_aircraft_pilots_on_flight_id", using: :btree
  add_index "flight_crew_role_aircraft_pilots", ["pilot_id"], name: "index_flight_crew_role_aircraft_pilots_on_pilot_id", using: :btree

  create_table "flight_type_crew_roles", force: :cascade do |t|
    t.integer  "flight_type_id"
    t.integer  "crew_role_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "flight_type_crew_roles", ["crew_role_id"], name: "index_flight_type_crew_roles_on_crew_role_id", using: :btree
  add_index "flight_type_crew_roles", ["flight_type_id"], name: "index_flight_type_crew_roles_on_flight_type_id", using: :btree

  create_table "flight_types", force: :cascade do |t|
    t.string   "identifier",                  null: false
    t.text     "aircraft_types", default: [],              array: true
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "flight_types", ["identifier"], name: "index_flight_types_on_identifier", unique: true, using: :btree

  create_table "flights", force: :cascade do |t|
    t.datetime "start_time",     null: false
    t.datetime "end_time",       null: false
    t.integer  "flight_type_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "flights", ["flight_type_id"], name: "index_flights_on_flight_type_id", using: :btree

  create_table "pilot_crew_roles", force: :cascade do |t|
    t.integer  "pilot_id"
    t.integer  "crew_role_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "pilot_crew_roles", ["crew_role_id"], name: "index_pilot_crew_roles_on_crew_role_id", using: :btree
  add_index "pilot_crew_roles", ["pilot_id"], name: "index_pilot_crew_roles_on_pilot_id", using: :btree

  create_table "pilots", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "account_id"
  end

  add_index "pilots", ["account_id"], name: "index_pilots_on_account_id", using: :btree

  add_foreign_key "flight_crew_role_aircraft_pilots", "aircrafts"
  add_foreign_key "flight_crew_role_aircraft_pilots", "crew_roles"
  add_foreign_key "flight_crew_role_aircraft_pilots", "flights"
  add_foreign_key "flight_crew_role_aircraft_pilots", "pilots"
  add_foreign_key "flight_type_crew_roles", "crew_roles"
  add_foreign_key "flight_type_crew_roles", "flight_types"
  add_foreign_key "flights", "flight_types"
  add_foreign_key "pilot_crew_roles", "crew_roles"
  add_foreign_key "pilot_crew_roles", "pilots"
  add_foreign_key "pilots", "accounts"
end
