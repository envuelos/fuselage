class AddAccountToPilot < ActiveRecord::Migration
  def change
    add_reference :pilots, :account, index: true, foreign_key: true
  end
end
