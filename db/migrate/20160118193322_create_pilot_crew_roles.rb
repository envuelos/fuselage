class CreatePilotCrewRoles < ActiveRecord::Migration
  def change
    create_table :pilot_crew_roles do |t|
      t.belongs_to :pilot, index: true, foreign_key: true
      t.belongs_to :crew_role, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
