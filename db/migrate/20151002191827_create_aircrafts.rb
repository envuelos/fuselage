class CreateAircrafts < ActiveRecord::Migration
  def change
    create_table :aircrafts do |t|
      t.string :brand
      t.string :model
      t.string :registration
      t.string :year
      t.string :type

      t.timestamps null: false
    end
  end
end
