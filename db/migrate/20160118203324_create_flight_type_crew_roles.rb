class CreateFlightTypeCrewRoles < ActiveRecord::Migration
  def change
    create_table :flight_type_crew_roles do |t|
      t.belongs_to :flight_type, index: true, foreign_key: true
      t.belongs_to :crew_role, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
