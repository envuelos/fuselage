class CreateFlightCrewRoleAircraftPilots < ActiveRecord::Migration
  def change
    create_table :flight_crew_role_aircraft_pilots do |t|
      t.belongs_to :flight, index: true, foreign_key: true
      t.belongs_to :crew_role, index: true, foreign_key: true
      t.belongs_to :aircraft, index: true, foreign_key: true
      t.belongs_to :pilot, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
