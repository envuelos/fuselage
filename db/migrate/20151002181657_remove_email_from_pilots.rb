class RemoveEmailFromPilots < ActiveRecord::Migration
  # An email belongs in an "account" class, it's not an inherent attribute of a
  # pilot.
  def change
    remove_column :pilots, :email, :string
  end
end
