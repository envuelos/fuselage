class CreateCrewRoles < ActiveRecord::Migration
  def change
    create_table :crew_roles do |t|
      t.string :identifier, null: false

      t.timestamps null: false
    end

    add_index :crew_roles, :identifier, unique: true
  end
end
