class AddAuthenticationTokenToAccount < ActiveRecord::Migration
  def change
    add_column :accounts, :authentication_token, :string, null: false

    add_index :accounts, :authentication_token, unique: true
  end
end
