class CreateFlights < ActiveRecord::Migration
  def change
    create_table :flights do |t|
      t.datetime :start_time, null: false
      t.datetime :end_time, null: false
      t.belongs_to :flight_type, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
