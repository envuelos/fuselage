class CreateFlightTypes < ActiveRecord::Migration
  def change
    create_table :flight_types do |t|
      t.string :identifier, null: false
      t.text :aircraft_types, array: true, default: []

      t.timestamps null: false
    end

    add_index :flight_types, :identifier, unique: true
  end
end
