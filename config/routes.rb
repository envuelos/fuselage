Rails.application.routes.draw do
  devise_for :accounts, controllers: { sessions: 'accounts/sessions' }

  api_version(
    module: 'V1',
    default: true,
    header:
      {
        name: 'Accept',
        value: 'application/vnd.landing.fuselage.v1+json'
      }
  ) do
    resources :pilots, only: [:index, :create, :show, :update]
    resources :gliders, only: [:index, :create, :show, :update]
    resources :airplanes, only: [:index, :create, :show, :update]
  end
end
