require 'rails_helper'

RSpec.describe 'V1 - Airplanes API', type: :request do
  let!(:account) { Account.create(email: 'hello@example.com', password: 'password', password_confirmation: 'password') }
  let!(:api_version_headers) { { 'Accept' => 'application/vnd.landing.fuselage.v1+json' } }
  let!(:auth_headers) { { 'Authorization' => "Token token=#{account.authentication_token}, email=#{account.email}" } }
  let(:full_headers) { api_version_headers.merge(auth_headers) }

  describe 'listing airplanes' do
    before do
      Airplane.create!(
        brand: 'Piper', model: 'PA-25 Pawnee', registration: 'LV-CNA'
      )
    end

    it 'returns the list of existing airplanes' do
      get airplanes_url, nil, full_headers
      expect(response).to be_ok
      parsed = JSON.parse(response.body).with_indifferent_access
      expect(parsed[:airplanes].first[:brand]).to eq('Piper')
      expect(parsed[:airplanes].first[:model]).to eq('PA-25 Pawnee')
      expect(parsed[:airplanes].first[:registration]).to eq('LV-CNA')
    end
  end
  describe 'showing a airplane' do
    let!(:airplane) do
      Airplane.create!(
        brand: 'Piper', model: 'PA-25 Pawnee', registration: 'LV-CNA'
      )
    end

    it 'returns the representation of a airplane' do
      get airplane_url(airplane), nil, full_headers
      expect(response).to be_ok
      parsed = JSON.parse(response.body).with_indifferent_access
      expect(parsed[:airplane][:brand]).to eq('Piper')
    end
  end
  describe 'creating a airplane' do
    it 'creates a new airplane given all fields are provided' do
      airplane_attributes = {
        airplane: {
          brand: 'Piper',
          model: 'PA-25 Pawnee',
          registration: 'LV-CNA'
        }
      }
      expect do
        post airplanes_url, airplane_attributes, full_headers
      end.to change { Airplane.count }.by(1)

      expect(response).to be_created
    end
  end
  describe 'updating a airplane' do
    let!(:airplane) do
      Airplane.create!(
        brand: 'Piper', model: 'PA-25 Pawnee', registration: 'LV-CNA'
      )
    end

    it 'can update a airplane attributes' do
      airplane_attributes = {
        airplane: {
          brand: 'LAT'
        }
      }

      expect do
        patch airplane_url(airplane), airplane_attributes, full_headers
      end.to_not change { Airplane.count }
      expect(response).to be_ok
      expect(Airplane.last.brand).to eq('LAT')
    end
  end
end
