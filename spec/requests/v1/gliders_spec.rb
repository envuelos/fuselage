require 'rails_helper'

RSpec.describe 'V1 - Gliders API', type: :request do
  let!(:account) { Account.create(email: 'hello@example.com', password: 'password', password_confirmation: 'password') }
  let!(:api_version_headers) { { 'Accept' => 'application/vnd.landing.fuselage.v1+json' } }
  let!(:auth_headers) { { 'Authorization' => "Token token=#{account.authentication_token}, email=#{account.email}" } }
  let(:full_headers) { api_version_headers.merge(auth_headers) }

  describe 'listing gliders' do
    before do
      Glider.create!(
        brand: 'LET', model: 'Blanik L23', registration: 'LV-ABC'
      )
    end

    it 'returns the list of existing gliders' do
      get gliders_url, nil, full_headers
      expect(response).to be_ok
      parsed = JSON.parse(response.body).with_indifferent_access
      expect(parsed[:gliders].first[:brand]).to eq('LET')
      expect(parsed[:gliders].first[:model]).to eq('Blanik L23')
      expect(parsed[:gliders].first[:registration]).to eq('LV-ABC')
    end
  end
  describe 'showing a glider' do
    let!(:glider) do
      Glider.create!(
        brand: 'LET', model: 'Blanik L23', registration: 'LV-ABC'
      )
    end

    it 'returns the representation of a glider' do
      get glider_url(glider), nil, full_headers
      expect(response).to be_ok
      parsed = JSON.parse(response.body).with_indifferent_access
      expect(parsed[:glider][:brand]).to eq('LET')
    end
  end
  describe 'creating a glider' do
    it 'creates a new glider given all fields are provided' do
      glider_attributes = {
        glider: {
          brand: 'LET',
          model: 'Blanik L23',
          registration: 'LV-ABC'
        }
      }
      expect do
        post gliders_url, glider_attributes, full_headers
      end.to change { Glider.count }.by(1)

      expect(response).to be_created
    end
  end
  describe 'updating a glider' do
    let!(:glider) do
      Glider.create!(
        brand: 'LET', model: 'Blanik L23', registration: 'LV-ABC'
      )
    end

    it 'can update a glider attributes' do
      glider_attributes = {
        glider: {
          brand: 'LAT'
        }
      }

      expect do
        patch glider_url(glider), glider_attributes, full_headers
      end.to_not change { Glider.count }
      expect(response).to be_ok
      expect(Glider.last.brand).to eq('LAT')
    end
  end
end
