require 'rails_helper'

RSpec.describe 'V1 - Pilots API', type: :request do
  let!(:account) { Account.create(email: 'hello@example.com', password: 'password', password_confirmation: 'password') }
  let!(:api_version_headers) { { 'Accept' => 'application/vnd.landing.fuselage.v1+json' } }
  let!(:auth_headers) { { 'Authorization' => "Token token=#{account.authentication_token}, email=#{account.email}" } }
  let(:full_headers) { api_version_headers.merge(auth_headers) }

  describe 'listing pilots' do
    before do
      Pilot.create!(
        first_name: 'John', last_name: 'Snow'
      )
    end

    it 'returns the list of existing pilots' do
      get pilots_url, nil, full_headers
      expect(response.status).to eq 200
      parsed = JSON.parse(response.body).with_indifferent_access
      expect(parsed[:pilots].first[:first_name]).to eq('John')
      expect(parsed[:pilots].first[:last_name]).to eq('Snow')
    end
  end
  describe 'showing a pilot' do
    let!(:pilot) do
      Pilot.create!(
        first_name: 'John', last_name: 'Snow'
      )
    end

    it 'returns the representation of a pilot' do
      get pilot_url(pilot), nil, full_headers
      expect(response.status).to eq 200
      parsed = JSON.parse(response.body).with_indifferent_access
      expect(parsed[:pilot][:first_name]).to eq('John')
    end
  end
  describe 'creating a pilot' do
    it 'creates a new pilot given all fields are provided' do
      pilot_attributes = {
        pilot: {
          first_name: 'Joe',
          last_name: 'Kittinger'
        }
      }
      expect do
        post pilots_url, pilot_attributes, full_headers
      end.to change { Pilot.count }.by(1)

      expect(response).to be_created
    end
  end
  describe 'updating a pilot' do
    let!(:pilot) do
      Pilot.create!(
        first_name: 'John', last_name: 'Snow'
      )
    end

    it 'can update a pilot attributes' do
      pilot_attributes = {
        pilot: {
          first_name: 'Not John',
          last_name: 'Not Snow'
        }
      }

      expect do
        patch pilot_url(pilot), pilot_attributes, full_headers
      end.to_not change { Pilot.count }
      expect(response.status).to eq 200
      expect(Pilot.last.first_name).to eq('Not John')
    end
  end
end
