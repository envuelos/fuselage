require 'rails_helper'

RSpec.describe Account, type: :model do
  it { is_expected.to have_one :pilot }
  it 'is assigned a random token on create' do
    account = Account.create!(email: 'hello@example.com',
                              password: 'password',
                              password_confirmation: 'password'
                             )

    expect(account.authentication_token).to_not be_empty
  end
end
