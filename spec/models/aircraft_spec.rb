require 'rails_helper'

RSpec.describe Aircraft, type: :model do
  it { is_expected.to validate_presence_of :brand }
  it { is_expected.to validate_presence_of :model }
  it { is_expected.to validate_presence_of :registration }
end
