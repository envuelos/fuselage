require 'rails_helper'

RSpec.describe FlightType, type: :model do
  it { is_expected.to validate_uniqueness_of :identifier }
  it { is_expected.to validate_presence_of :identifier }
  it { is_expected.to have_many :flight_type_crew_roles }
  it { is_expected.to have_many(:crew_roles).through(:flight_type_crew_roles) }
end
