require 'rails_helper'

RSpec.describe Flight, type: :model do
  it { is_expected.to belong_to :flight_type }
end
