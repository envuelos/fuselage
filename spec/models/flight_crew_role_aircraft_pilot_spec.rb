require 'rails_helper'

RSpec.describe FlightCrewRoleAircraftPilot, type: :model do
  it { is_expected.to belong_to(:flight) }
  it { is_expected.to belong_to(:crew_role) }
  it { is_expected.to belong_to(:aircraft) }
  it { is_expected.to belong_to(:pilot) }
end
